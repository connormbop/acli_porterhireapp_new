CREATE DATABASE QUOTING
GO

USE QUOTING

CREATE TABLE CLIENTS
(
CustomerID int IDENTITY,
C_FirstName varchar(25) NOT NULL,
C_LastName varchar(50) NOT NULL,
C_Email varchar(50) NOT NULL,
C_CompanyName varchar(50) NOT NULL,
C_Phone varchar(10) NOT NULL,
primary key(CustomerID, C_FirstName, C_LastName, C_Email),

CHECK(C_Email LIKE '%___@___%'),
CHECK(C_Phone NOT LIKE '%[^0-9]%'),
CHECK(C_FirstName NOT LIKE '%[^A-Z ]%'),
CHECK(C_LastName NOT LIKE '%[^A-Z ]%'),
CHECK(C_CompanyName NOT LIKE '%[^A-Z ]%'),
CHECK(DATALENGTH([C_Phone]) > 7)
)


CREATE TABLE ADMINS
(
AdminID int IDENTITY,
A_Username varchar(25) NOT NULL,
A_FirstName varchar(25) NOT NULL,
A_LastName varchar(50) NOT NULL,
A_Email varchar(50) NOT NULL,
A_Phone varchar(10) NOT NULL,
A_Password varchar(25) NOT NULL,

CHECK(A_Email LIKE '%___@___%'),
CHECK(A_Phone not like '%[^0-9]%'),
CHECK(A_FirstName NOT LIKE '%[^A-Z ]%'),
CHECK(A_LastName NOT LIKE '%[^A-Z ]%'),
CHECK(A_Username NOT LIKE '% %'),
CHECK(DATALENGTH([A_Password]) > 6),
)


CREATE TABLE PRODUCTS
(
ProductID int IDENTITY,
ProductDescription varchar(50),
ProductMake varchar(25),
ProductModel varchar(25),
ProductPriceDay money,
ProductPriceWeek money,
ProductPriceMonth money,
StockLevel int,
FuelType varchar(20),
primary key(ProductID, ProductPriceDay, ProductPriceWeek, ProductPriceMonth),

CHECK(FuelType = 'Petrol' OR FuelType = 'Diesel')
)

CREATE TABLE USERS
(
UserID int IDENTITY NOT NULL,
U_Username varchar(25) NOT NULL,
U_FirstName varchar(25) NOT NULL,
U_LastName varchar(50) NOT NULL,
U_Email varchar(50) NOT NULL,
U_Phone varchar(10) NOT NULL,
U_Password varchar(50) NOT NULL,
primary key(UserID, U_FirstName),

CHECK(U_Email LIKE '%___@___%'),
CHECK(U_Phone not like '%[^0-9]%'),
CHECK(U_FirstName NOT LIKE '%[^A-Z ]%'),
CHECK(U_LastName NOT LIKE '%[^A-Z ]%'),
CHECK(U_Username NOT LIKE '% %'),
CHECK(DATALENGTH([U_Password]) > 6)  
)

CREATE TABLE FUEL
(
FuelType varchar(20) NOT NULL,
FuelPrice float NOT NULL,
primary key(FuelType, FuelPrice),

CHECK(FuelType = 'Petrol' OR FuelType = 'Diesel')
)

CREATE TABLE QUOTES
(
ReferenceID int IDENTITY NOT NULL,
CID int NOT NULL,
CFN varchar(25) NOT NULL,
CLN varchar(50) NOT NULL,
CE varchar(50) NOT NULL,
PID int NOT NULL,
PriceD MONEY NOT NULL,
PriceW MONEY NOT NULL,
PriceM MONEY NOT NULL,
FType varchar(20) NOT NULL,
FC float NOT NULL,
StaffID int NOT NULL,
UFN varchar(25) NOT NULL,
FuelQuantityLitre float NOT NULL,
GST float NOT NULL,
INSURANCE FLOAT NOT NULL,
TotalMinusGST float NOT NULL,
TotalPlusGST float NOT NULL,
DateCreated date NOT NULL,
HiredDays int NOT NULL,
AcceptedORNotAccepted varchar(15) NOT NULL,
foreign key (CID,CFN,CLN,CE) references CLIENTS(CustomerID, C_FirstName, C_LastName, C_Email),
foreign key (PID, PriceD, PriceW, PriceM) references PRODUCTS(ProductID, ProductPriceDay, ProductPriceWeek, ProductPriceMonth),
foreign key (FType, FC) references FUEL(FuelType, FuelPrice),
foreign key (StaffID, UFN) references USERS(UserID, U_FirstName),
Primary Key (ReferenceID),

CHECK(CE LIKE '%___@___%'),
CHECK(CFN NOT LIKE '%[^A-Z ]%'),
CHECK(CLN NOT LIKE '%[^A-Z ]%'),
CHECK(UFN NOT LIKE '%[^A-Z ]%'),
CHECK(AcceptedORNotAccepted = 'Accepted' OR AcceptedORNotAccepted = 'Not Accepted'),
)

Create TABLE QUOTESTATUS
(
RefID int IDENTITY NOT NULL,
CustomerID int NOT NULL,
Status VARCHAR(25) NOT NULL,
foreign key (RefID) references QUOTES (ReferenceID),
)


SELECT * FROM QUOTES
SELECT * FROM CLIENTS
SELECT * FROM FUEL
SELECT * FROM USERS
SELECT * FROM ADMINS
SELECT * FROM PRODUCTS
SELECT * FROM QUOTESTATUS

DROP TABLE PRODUCTS
DROP TABLE ADMINS
DROP TABLE USERS
DROP TABLE FUEL
DROP TABLE CLIENTS
DROP TABLE QUOTES
DROP TABLE QUOTESTATUS

INSERT into CLIENTS(C_FirstName, C_LastName, C_Email, C_CompanyName, C_Phone) Values ('Connor', 'MacPherson', 'connormbop@gmail.com', 'CEarthMovers', '0220272522')
INSERT into CLIENTS(C_FirstName, C_LastName, C_Email, C_CompanyName, C_Phone) Values ('Luke', 'Johnston', 'lukejohnston@gmail.com', 'LEarthMovers', '0220754198')
INSERT into CLIENTS(C_FirstName, C_LastName, C_Email, C_CompanyName, C_Phone) Values ('Igncio', 'GattiBlair', 'ignciogatti@gmail.com', 'IINC', '0220276588')
INSERT into CLIENTS(C_FirstName, C_LastName, C_Email, C_CompanyName, C_Phone) Values ('Amy', 'MacIntyre', 'amymac@gmail.com', 'AEarthworks', '0226782522')

INSERT into ADMINS(A_Username, A_FirstName, A_LastName, A_Email, A_Phone, A_Password) Values ('AmyMc', 'Amy', 'McIntyre', 'AmyMac@gmail.com', '0215890236', 'AmyIsGay123')
INSERT into ADMINS(A_Username, A_FirstName, A_LastName, A_Email, A_Phone, A_Password) Values ('Administrator', 'Super', 'User', 'Administrator@gmail.com', '0220754198', 'Administrator')

INSERT INTO QUOTES values (2 ,'Tester', 'Client', 'customer1@hotmail.com', 1, 'Big Digger', 200.00, 'Diesel', 1.50, 1, 'Testuser', 20, 0.15, 0.10,200.00, 230.00, '2017/08/19', 'Not Accepted')

--QUOTES INSERT START--
INSERT INTO QUOTES (CID, CFN, CLN, CE, PID, PriceD, PriceW, PriceM, FType, FC, StaffID, UFN, FuelQuantityLitre, GST, INSURANCE, TotalMinusGST, TotalPlusGST, DateCreated,HiredDays, AcceptedORNotAccepted) 
values (
(SELECT CustomerID FROM CLIENTS WHERE CustomerID = 1),
(SELECT C_FirstName FROM CLIENTS WHERE CustomerID = 1),
(SELECT C_LastName FROM CLIENTS WHERE CustomerID = 1),
(SELECT C_Email FROM CLIENTS WHERE CustomerID = 1),
(SELECT ProductID from PRODUCTS where ProductID = 1),
(SELECT ProductPriceDay FROM PRODUCTS WHERE ProductID = 1),
(SELECT ProductPriceWeek FROM PRODUCTS WHERE ProductID = 1),
(SELECT ProductPriceMonth FROM PRODUCTS WHERE ProductID = 1),
(SELECT FuelType from FUEL where FuelType = 'Diesel'),
(SELECT FuelPrice from FUEL where FuelType = 'Diesel'),
(SELECT UserID FROM USERS where UserID = 1), 
(SELECT U_FirstName from USERS where UserID = 1), 20, 0.15, 0.10,200.00, 230.00, '2017/08/19', 5, 'Not Accepted')
--QUOTES INSERT FINISH--

INSERT into FUEL values ('Petrol', 2.50)
INSERT into FUEL values ('Diesel', 1.99)

INSERT into USERS(U_Username, U_FirstName, U_LastName, U_Email, U_Phone, U_Password) Values ('Luke123', 'Luke', 'Johnston', 'LukeJ@hotmail.com', '0220754198', 'LJ12345')
INSERT into USERS(U_Username, U_FirstName, U_LastName, U_Email, U_Phone, U_Password) Values ('Ignacio456', 'Ignacio', 'GattiBlair', 'Ignacio@hotmail.com', '0275983566', 'IGB12345')


INSERT INTO PRODUCTS VALUES('Excavators Wheeled', 'Hyundai', 'R140-7', 385, 1694, 6048, 1, 'Diesel');
INSERT INTO PRODUCTS VALUES('Excavators Wheeled', 'Hitachi', 'EX60WD', 540, 2376, 8482, 1, 'Diesel');
INSERT INTO PRODUCTS VALUES('Excavators Wheeled','Doosan', 'DX140WD', 540, 2376, 8482, 1, 'Diesel');
INSERT INTO PRODUCTS VALUES('Excavators Wheeled','Hyundai', 'R140W-9', 577, 2539, 9064, 1, 'Diesel');

INSERT INTO PRODUCTS VALUES('Excavators Demolition', 'Hyundai', 'R210LC-7H/C', 695, 3058, 10917, 1 , 'Diesel');
INSERT INTO PRODUCTS VALUES('Excavators Demolition', 'Hyundai', 'R210LC-9', 695, 3058, 10917, 1 , 'Diesel');

INSERT INTO PRODUCTS VALUES('Excavator Forestry', 'Hyundai', 'R211LC-7H/C', 695, 3058, 10917, 1 , 'Diesel');
INSERT INTO PRODUCTS VALUES('Excavator Forestry', 'Hyundai', 'R210LC-9H/C', 745, 3278, 11702, 1 , 'Diesel');
INSERT INTO PRODUCTS VALUES('Excavator Forestry', 'Komatsu', 'PC220-7', 695, 3058, 10917, 1 , 'Diesel');
INSERT INTO PRODUCTS VALUES('Excavator Forestry', 'Volvo', 'EC240', 695, 3058, 10917, 1, 'Diesel');
INSERT INTO PRODUCTS VALUES('Excavator Forestry','Hyundai', 'R250LC-9H', 787, 3463, 12362, 1, 'Diesel');

INSERT INTO PRODUCTS VALUES('Excavators RockBreakers', 'Furukawa', 'F2', 399, 1756, 6267, 1 , 'Diesel');
INSERT INTO PRODUCTS VALUES('Excavators RockBreakers', 'Furukawa', 'F3', 420, 1848, 6597, 1 , 'Diesel');
INSERT INTO PRODUCTS VALUES('Excavators RockBreakers', 'Furukawa', 'F5', 520, 2288, 8168, 1, 'Diesel');
INSERT INTO PRODUCTS VALUES('Excavators RockBreakers', 'Furukawa', 'F6', 630, 2772, 9896, 1, 'Diesel');
INSERT INTO PRODUCTS VALUES('Excavators RockBreakers', 'Furukawa', 'F12', 840, 3696, 13195, 1, 'Diesel');
INSERT INTO PRODUCTS VALUES('Excavators RockBreakers', 'Furukawa', 'F19', 1200, 5280, 18850, 1 , 'Diesel');
INSERT INTO PRODUCTS VALUES('Excavators RockBreakers', 'Furukawa', 'F22', 1200, 5280, 18850, 1 , 'Diesel');
INSERT INTO PRODUCTS VALUES('Excavators RockBreakers', 'Furukawa', 'F35', 1600, 7040, 25133, 1 , 'Diesel');

INSERT INTO PRODUCTS VALUES('Tractor Brooms','John Deere', 'LV3520', 325, 1430, 5105, 2, 'Petrol');
INSERT INTO PRODUCTS VALUES('Tractor Brooms','John Deere', 'LV4720', 325, 1430, 5105, 1, 'Petrol');
INSERT INTO PRODUCTS VALUES('Tractor Brooms', 'Kubota', 'M5700', 365, 1606, 5733, 3, 'Petrol');
INSERT INTO PRODUCTS VALUES('Tractor Brooms', 'Kubota', 'L5030', 365, 1606, 5733, 3, 'Petrol');
INSERT INTO PRODUCTS VALUES('Tractor Brooms', 'Laymor', 'Towable', 325, 1430, 5105, 1, 'Petrol');

INSERT INTO PRODUCTS VALUES('Rollers Twin Smooth Drum','Bomag', 'BW90', 165, 730, 2606, 1, 'Diesel');
INSERT INTO PRODUCTS VALUES('Rollers Twin Smooth Drum', 'Bomag', 'BW80AD-2', 165, 730, 2602, 1, 'Diesel');
INSERT INTO PRODUCTS VALUES('Rollers Twin Smooth Drum','Hamm', 'HD10CVV', 180, 790, 2820, 1, 'Diesel');
INSERT INTO PRODUCTS VALUES('Rollers Twin Smooth Drum','Bomag', 'BW100Adm-5', 208, 927, 3280, 1, 'Diesel');
INSERT INTO PRODUCTS VALUES('Rollers Twin Smooth Drum', 'Sakai', 'SW200', 215, 940, 3356, 1, 'Diesel');
INSERT INTO PRODUCTS VALUES('Rollers Twin Smooth Drum', 'Hamm', 'HD10VV', 230, 1000, 3570, 1 , 'Diesel');
INSERT INTO PRODUCTS VALUES('Rollers Twin Smooth Drum', 'Sakai', 'SW25', 260, 1160, 4141, 1, 'Diesel');
INSERT INTO PRODUCTS VALUES('Rollers Twin Smooth Drum', 'Dynapac', 'CC122C', 260, 1160, 4141, 1, 'Diesel');
INSERT INTO PRODUCTS VALUES('Rollers Twin Smooth Drum', 'Dynapac', 'RV30', 260, 1160, 4141, 1, 'Diesel');
INSERT INTO PRODUCTS VALUES('Rollers Twin Smooth Drum', 'Vibracom', 'TD25', 260, 1160, 4141, 1, 'Diesel');
INSERT INTO PRODUCTS VALUES('Rollers Twin Smooth Drum', 'Sakai', 'SW350', 275, 1210, 4320, 1, 'Diesel');
INSERT INTO PRODUCTS VALUES('Rollers Twin Smooth Drum', 'Sakai', 'SW500', 315, 1375, 4909, 1, 'Diesel');
INSERT INTO PRODUCTS VALUES('Rollers Twin Smooth Drum', 'Dynapac', 'CC142', 315, 1375, 4909, 1, 'Diesel');
INSERT INTO PRODUCTS VALUES('Rollers Twin Smooth Drum', 'Vibracom', 'TD40', 315, 1375, 4909, 1, 'Diesel');
INSERT INTO PRODUCTS VALUES('Rollers Twin Smooth Drum', 'Bomag', 'BW120AD-5', 315, 1375, 4909, 1, 'Diesel');
INSERT INTO PRODUCTS VALUES('Rollers Twin Smooth Drum', 'Sakai', 'SW60', 390, 1695, 6051, 1 , 'Diesel');
INSERT INTO PRODUCTS VALUES('Rollers Twin Smooth Drum', 'Sakai', 'SW70C', 390, 1695, 6051, 1, 'Diesel');
INSERT INTO PRODUCTS VALUES('Rollers Twin Smooth Drum', 'Dynapac', 'CC222', 445, 1895, 6765, 1, 'Diesel');
INSERT INTO PRODUCTS VALUES('Rollers Twin Smooth Drum', 'Bomag', 'BW141-2', 445,1895, 6765, 1 , 'Diesel');
INSERT INTO PRODUCTS VALUES('Rollers Twin Smooth Drum', 'Sakai', 'SW652', 445, 1895, 6765, 1, 'Diesel');
INSERT INTO PRODUCTS VALUES('Rollers Twin Smooth Drum', 'Hamm', 'HD75', 445, 1895, 6765, 1, 'Diesel');
INSERT INTO PRODUCTS VALUES('Rollers Twin Smooth Drum', 'Bomag', 'BW151-2', 460, 2000, 7140, 1, 'Diesel');
INSERT INTO PRODUCTS VALUES('Rollers Twin Smooth Drum', 'Bomag', 'BW151AD-5AM', 622, 2675, 9425, 1, 'Diesel');
INSERT INTO PRODUCTS VALUES('Rollers Twin Smooth Drum', 'Sakai', 'SW750', 485, 2155, 7693, 1, 'Diesel');
INSERT INTO PRODUCTS VALUES('Rollers Twin Smooth Drum', 'Sakai', 'SW100', 485, 2155, 7693, 1, 'Diesel');
INSERT INTO PRODUCTS VALUES('Rollers Twin Smooth Drum', 'Hamm', 'HD110', 535, 2365, 8443, 1 , 'Diesel');

INSERT INTO PRODUCTS VALUES('Rollers Single Drum Padfoot', 'Bomag', 'BW124DH-4', 488, 2168, 7700, 1, 'Diesel');
INSERT INTO PRODUCTS VALUES('Rollers Single Drum Padfoot','Sakai', 'SV200TB', 340, 1495, 5337, 1, 'Diesel');
INSERT INTO PRODUCTS VALUES('Rollers Single Drum Padfoot','Sakai', 'SV201TF', 340, 1495, 5337, 1, 'Diesel');
INSERT INTO PRODUCTS VALUES('Rollers Single Drum Padfoot','Volvo', 'SD45D', 340, 1495, 5337, 1, 'Diesel');
INSERT INTO PRODUCTS VALUES('Rollers Single Drum Padfoot','Hamm', '3205', 390, 1700, 6069, 1, 'Diesel');
INSERT INTO PRODUCTS VALUES('Rollers Single Drum Padfoot','Sakai', 'SV70TB', 485, 2150, 7676, 1, 'Diesel');
INSERT INTO PRODUCTS VALUES('Rollers Single Drum Padfoot','Vibracom', 'VS80', 485, 2150, 7676, 1, 'Diesel');
INSERT INTO PRODUCTS VALUES('Rollers Single Drum Padfoot','Sakai', 'SV400TF', 490, 2200, 7854, 1, 'Diesel');
INSERT INTO PRODUCTS VALUES('Rollers Single Drum Padfoot','Hamm', '3307', 490, 2200, 7854, 1, 'Diesel');
INSERT INTO PRODUCTS VALUES('Rollers Single Drum Padfoot','Sakai', 'SV500DF', 510, 2270, 8104, 1, 'Diesel');
INSERT INTO PRODUCTS VALUES('Rollers Single Drum Padfoot','Vibracom', 'VS120', 535, 2385, 8514, 1, 'Diesel');
INSERT INTO PRODUCTS VALUES('Rollers Single Drum Padfoot','Hamm', '3410', 610, 2470, 8818, 1, 'Diesel');

INSERT INTO PRODUCTS VALUES('Rollers 3 Point Static', 'Sakai', 'R2S', 395, 1795, 6408, 1, 'Diesel');
INSERT INTO PRODUCTS VALUES('Rollers 3 Point Static', 'Komatsu', 'JM120', 395, 1795, 6408, 1, 'Diesel');
INSERT INTO PRODUCTS VALUES('Rollers 3 Point Static', 'Dynapac', 'CS12', 395, 1795, 6408, 1, 'Diesel');

INSERT INTO PRODUCTS VALUES('Bulldozers STD Track', 'Komatsu', 'D31E-20', 435, 1933, 6900, 1, 'Diesel');
INSERT INTO PRODUCTS VALUES('Bulldozers STD Track', 'Komatsu', 'D39EX-21', 618, 2555, 8250, 1, 'Diesel');
INSERT INTO PRODUCTS VALUES('Bulldozers STD Track', 'Cat', 'D5M', 705, 3130, 11175, 1, 'Diesel');
INSERT INTO PRODUCTS VALUES('Bulldozers STD Track', 'Cat', 'D5K', 749, 3328, 11880, 1, 'Diesel');
INSERT INTO PRODUCTS VALUES('Bulldozers STD Track', 'Komatsu', 'D51EX-22', 749, 3328, 11880, 1, 'Diesel');
INSERT INTO PRODUCTS VALUES('Bulldozers STD Track', 'Komatsu', 'D61EX-12', 724, 3214, 11475, 1, 'Diesel');
INSERT INTO PRODUCTS VALUES('Bulldozers STD Track', 'Cat', 'D6M', 724, 3214, 11475, 1, 'Diesel');
INSERT INTO PRODUCTS VALUES('Bulldozers STD Track', 'Cat', 'D6T', 1020, 4410, 15600, 1, 'Diesel');
INSERT INTO PRODUCTS VALUES('Bulldozers STD Track', 'Komatsu', 'D61EX-15', 749, 3328, 11880, 1, 'Diesel');
INSERT INTO PRODUCTS VALUES('Bulldozers STD Track', 'Komatsu', 'D41E-6', 666, 2958, 10560,1 , 'Diesel');

INSERT INTO PRODUCTS VALUES('Bulldozers Swamp Track', 'Komatsu', 'D31P-20', 443, 1950, 6960, 1, 'Diesel');
INSERT INTO PRODUCTS VALUES('Bulldozers Swamp Track', 'Komatsu', 'D31P-21', 443, 1950, 6960, 1, 'Diesel');
INSERT INTO PRODUCTS VALUES('Bulldozers Swamp Track', 'Komatsu', 'D37P-5', 504, 2218, 7920, 1, 'Diesel');
INSERT INTO PRODUCTS VALUES('Bulldozers Swamp Track', 'Komatsu', 'D37PX-21', 590, 2730, 9625, 1, 'Diesel');
INSERT INTO PRODUCTS VALUES('Bulldozers Swamp Track', 'Cat', 'D5N', 710, 3126, 11160, 1, 'Diesel');
INSERT INTO PRODUCTS VALUES('Bulldozers Swamp Track', 'Cat', 'D6N', 733, 3227, 11520, 1, 'Diesel');
INSERT INTO PRODUCTS VALUES('Bulldozers Swamp Track', 'Cat', 'D5K', 733, 3227, 11520, 1, 'Diesel');
INSERT INTO PRODUCTS VALUES('Bulldozers Swamp Track', 'Cat', 'D6R', 825, 3630, 12960, 1, 'Diesel');

INSERT INTO PRODUCTS VALUES('Bulldozers and Hydraulic Scoops', 'Kokudo', '15SB',340, 1495, 5337, 1, 'Diesel');
INSERT INTO PRODUCTS VALUES('Bulldozers and Hydraulic Scoops','Kokudo', 'D85A',340, 1495, 5337, 1, 'Diesel');

INSERT INTO PRODUCTS VALUES('Loaders ToolHandler', 'Volvo', 'L25B', 310, 1364, 4869, 1, 'Diesel');
INSERT INTO PRODUCTS VALUES('Loaders ToolHandler', 'Cat', '906H', 340, 1477, 5150, 1, 'Diesel');
INSERT INTO PRODUCTS VALUES('Loaders ToolHandler', 'TCM', 'L9-2', 340, 1477, 5150, 1, 'Diesel');
INSERT INTO PRODUCTS VALUES('Loaders ToolHandler', 'Komatsu', 'WA80', 340, 1477, 5150, 1, 'Diesel');
INSERT INTO PRODUCTS VALUES('Loaders ToolHandler', 'Komatsu', 'WA100M', 340, 1477, 5150, 1, 'Diesel');
INSERT INTO PRODUCTS VALUES('Loaders ToolHandler', 'Volvo', 'L45', 394, 1635, 5888, 1, 'Diesel');
INSERT INTO PRODUCTS VALUES('Loaders ToolHandler', 'Hyundai', 'HL740TM-7', 430, 1892, 6754, 1, 'Diesel');
INSERT INTO PRODUCTS VALUES('Loaders ToolHandler', 'Hyundai', 'HL740TM-9', 455, 2002, 7147, 1, 'Diesel');
INSERT INTO PRODUCTS VALUES('Loaders ToolHandler', 'Hyundai', 'HM740TM-7', 470, 2068, 7383, 1, 'Diesel');
INSERT INTO PRODUCTS VALUES('Loaders ToolHandler', 'Hyundai', 'HM740TM-9', 535, 2270, 7883, 1, 'Diesel');
INSERT INTO PRODUCTS VALUES('Loaders ToolHandler', 'Volvo', 'L120', 593, 2785, 8760, 1, 'Diesel');

INSERT INTO PRODUCTS VALUES('LogLoaders', 'Cat', '950G', 648, 2851, 10179, 1, 'Diesel');
INSERT INTO PRODUCTS VALUES('LogLoaders', 'JCB', '436', 648, 2851, 10179, 1, 'Diesel');
INSERT INTO PRODUCTS VALUES('LogLoaders', 'JCB', '456', 725, 3190, 11388, 1, 'Diesel');
INSERT INTO PRODUCTS VALUES('LogLoaders', 'Hyundai', 'HL770-9', 756, 3326, 11875, 1, 'Diesel');