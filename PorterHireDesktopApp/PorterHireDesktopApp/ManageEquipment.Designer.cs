﻿namespace PorterHireDesktopApp
{
    partial class ManageEquipment
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblStockName = new System.Windows.Forms.Label();
            this.lblStockDescription = new System.Windows.Forms.Label();
            this.StockName = new System.Windows.Forms.TextBox();
            this.StockDescr = new System.Windows.Forms.RichTextBox();
            this.lblManageStock = new System.Windows.Forms.Label();
            this.listStock = new System.Windows.Forms.ListBox();
            this.btnSave = new System.Windows.Forms.Button();
            this.btnDelete = new System.Windows.Forms.Button();
            this.lblStockPriceFuelType = new System.Windows.Forms.Label();
            this.lblStockLevel = new System.Windows.Forms.Label();
            this.lblStockPriceMonth = new System.Windows.Forms.Label();
            this.lblStockPriceWeek = new System.Windows.Forms.Label();
            this.lblEquipPriceDay = new System.Windows.Forms.Label();
            this.lblStockModel = new System.Windows.Forms.Label();
            this.lblStockMake = new System.Windows.Forms.Label();
            this.StockPriceWeek = new System.Windows.Forms.TextBox();
            this.StockPriceDay = new System.Windows.Forms.TextBox();
            this.StockModel = new System.Windows.Forms.TextBox();
            this.StockMake = new System.Windows.Forms.TextBox();
            this.StockLevel = new System.Windows.Forms.TextBox();
            this.StockPriceMonth = new System.Windows.Forms.TextBox();
            this.StockFuelType = new System.Windows.Forms.ComboBox();
            this.StockEdit = new System.Windows.Forms.Button();
            this.StockBack = new System.Windows.Forms.Button();
            this.txtStockMax = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // lblStockName
            // 
            this.lblStockName.AutoSize = true;
            this.lblStockName.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.lblStockName.Location = new System.Drawing.Point(54, 95);
            this.lblStockName.Name = "lblStockName";
            this.lblStockName.Size = new System.Drawing.Size(51, 20);
            this.lblStockName.TabIndex = 2;
            this.lblStockName.Text = "Name";
            // 
            // lblStockDescription
            // 
            this.lblStockDescription.AutoSize = true;
            this.lblStockDescription.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.lblStockDescription.Location = new System.Drawing.Point(54, 139);
            this.lblStockDescription.Name = "lblStockDescription";
            this.lblStockDescription.Size = new System.Drawing.Size(89, 20);
            this.lblStockDescription.TabIndex = 7;
            this.lblStockDescription.Text = "Description";
            // 
            // StockName
            // 
            this.StockName.Location = new System.Drawing.Point(111, 95);
            this.StockName.Name = "StockName";
            this.StockName.Size = new System.Drawing.Size(100, 20);
            this.StockName.TabIndex = 9;
            // 
            // StockDescr
            // 
            this.StockDescr.Location = new System.Drawing.Point(169, 139);
            this.StockDescr.Name = "StockDescr";
            this.StockDescr.Size = new System.Drawing.Size(297, 49);
            this.StockDescr.TabIndex = 15;
            this.StockDescr.Text = "";
            this.StockDescr.TextChanged += new System.EventHandler(this.StockDescr_TextChanged);
            // 
            // lblManageStock
            // 
            this.lblManageStock.AutoSize = true;
            this.lblManageStock.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.lblManageStock.Location = new System.Drawing.Point(12, 9);
            this.lblManageStock.Name = "lblManageStock";
            this.lblManageStock.Size = new System.Drawing.Size(112, 20);
            this.lblManageStock.TabIndex = 16;
            this.lblManageStock.Text = "Manage Stock";
            // 
            // listStock
            // 
            this.listStock.FormattingEnabled = true;
            this.listStock.Location = new System.Drawing.Point(579, 43);
            this.listStock.Name = "listStock";
            this.listStock.Size = new System.Drawing.Size(377, 641);
            this.listStock.TabIndex = 17;
            // 
            // btnSave
            // 
            this.btnSave.Location = new System.Drawing.Point(58, 700);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(75, 23);
            this.btnSave.TabIndex = 20;
            this.btnSave.Text = "Save";
            this.btnSave.UseVisualStyleBackColor = true;
            // 
            // btnDelete
            // 
            this.btnDelete.Location = new System.Drawing.Point(391, 700);
            this.btnDelete.Name = "btnDelete";
            this.btnDelete.Size = new System.Drawing.Size(75, 23);
            this.btnDelete.TabIndex = 21;
            this.btnDelete.Text = "Delete";
            this.btnDelete.UseVisualStyleBackColor = true;
            // 
            // lblStockPriceFuelType
            // 
            this.lblStockPriceFuelType.AutoSize = true;
            this.lblStockPriceFuelType.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.lblStockPriceFuelType.Location = new System.Drawing.Point(54, 618);
            this.lblStockPriceFuelType.Name = "lblStockPriceFuelType";
            this.lblStockPriceFuelType.Size = new System.Drawing.Size(78, 20);
            this.lblStockPriceFuelType.TabIndex = 22;
            this.lblStockPriceFuelType.Text = "Fuel Type";
            // 
            // lblStockLevel
            // 
            this.lblStockLevel.AutoSize = true;
            this.lblStockLevel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.lblStockLevel.Location = new System.Drawing.Point(54, 578);
            this.lblStockLevel.Name = "lblStockLevel";
            this.lblStockLevel.Size = new System.Drawing.Size(91, 20);
            this.lblStockLevel.TabIndex = 23;
            this.lblStockLevel.Text = "Stock Level";
            // 
            // lblStockPriceMonth
            // 
            this.lblStockPriceMonth.AutoSize = true;
            this.lblStockPriceMonth.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.lblStockPriceMonth.Location = new System.Drawing.Point(54, 514);
            this.lblStockPriceMonth.Name = "lblStockPriceMonth";
            this.lblStockPriceMonth.Size = new System.Drawing.Size(103, 20);
            this.lblStockPriceMonth.TabIndex = 24;
            this.lblStockPriceMonth.Text = "Price (Month)";
            // 
            // lblStockPriceWeek
            // 
            this.lblStockPriceWeek.AutoSize = true;
            this.lblStockPriceWeek.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.lblStockPriceWeek.Location = new System.Drawing.Point(54, 447);
            this.lblStockPriceWeek.Name = "lblStockPriceWeek";
            this.lblStockPriceWeek.Size = new System.Drawing.Size(99, 20);
            this.lblStockPriceWeek.TabIndex = 25;
            this.lblStockPriceWeek.Text = "Price (Week)";
            // 
            // lblEquipPriceDay
            // 
            this.lblEquipPriceDay.AutoSize = true;
            this.lblEquipPriceDay.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.lblEquipPriceDay.Location = new System.Drawing.Point(54, 379);
            this.lblEquipPriceDay.Name = "lblEquipPriceDay";
            this.lblEquipPriceDay.Size = new System.Drawing.Size(86, 20);
            this.lblEquipPriceDay.TabIndex = 26;
            this.lblEquipPriceDay.Text = "Price (Day)";
            // 
            // lblStockModel
            // 
            this.lblStockModel.AutoSize = true;
            this.lblStockModel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.lblStockModel.Location = new System.Drawing.Point(54, 308);
            this.lblStockModel.Name = "lblStockModel";
            this.lblStockModel.Size = new System.Drawing.Size(52, 20);
            this.lblStockModel.TabIndex = 27;
            this.lblStockModel.Text = "Model";
            // 
            // lblStockMake
            // 
            this.lblStockMake.AutoSize = true;
            this.lblStockMake.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.lblStockMake.Location = new System.Drawing.Point(54, 233);
            this.lblStockMake.Name = "lblStockMake";
            this.lblStockMake.Size = new System.Drawing.Size(48, 20);
            this.lblStockMake.TabIndex = 28;
            this.lblStockMake.Text = "Make";
            // 
            // StockPriceWeek
            // 
            this.StockPriceWeek.Location = new System.Drawing.Point(159, 447);
            this.StockPriceWeek.Name = "StockPriceWeek";
            this.StockPriceWeek.Size = new System.Drawing.Size(100, 20);
            this.StockPriceWeek.TabIndex = 29;
            // 
            // StockPriceDay
            // 
            this.StockPriceDay.Location = new System.Drawing.Point(146, 379);
            this.StockPriceDay.Name = "StockPriceDay";
            this.StockPriceDay.Size = new System.Drawing.Size(100, 20);
            this.StockPriceDay.TabIndex = 30;
            // 
            // StockModel
            // 
            this.StockModel.Location = new System.Drawing.Point(111, 308);
            this.StockModel.Name = "StockModel";
            this.StockModel.Size = new System.Drawing.Size(100, 20);
            this.StockModel.TabIndex = 31;
            // 
            // StockMake
            // 
            this.StockMake.Location = new System.Drawing.Point(111, 233);
            this.StockMake.Name = "StockMake";
            this.StockMake.Size = new System.Drawing.Size(100, 20);
            this.StockMake.TabIndex = 32;
            // 
            // StockLevel
            // 
            this.StockLevel.Location = new System.Drawing.Point(159, 578);
            this.StockLevel.Name = "StockLevel";
            this.StockLevel.Size = new System.Drawing.Size(100, 20);
            this.StockLevel.TabIndex = 33;
            // 
            // StockPriceMonth
            // 
            this.StockPriceMonth.Location = new System.Drawing.Point(159, 514);
            this.StockPriceMonth.Name = "StockPriceMonth";
            this.StockPriceMonth.Size = new System.Drawing.Size(100, 20);
            this.StockPriceMonth.TabIndex = 34;
            // 
            // StockFuelType
            // 
            this.StockFuelType.FormattingEnabled = true;
            this.StockFuelType.Location = new System.Drawing.Point(159, 616);
            this.StockFuelType.Name = "StockFuelType";
            this.StockFuelType.Size = new System.Drawing.Size(121, 21);
            this.StockFuelType.TabIndex = 35;
            // 
            // StockEdit
            // 
            this.StockEdit.Location = new System.Drawing.Point(579, 700);
            this.StockEdit.Name = "StockEdit";
            this.StockEdit.Size = new System.Drawing.Size(75, 23);
            this.StockEdit.TabIndex = 36;
            this.StockEdit.Text = "Edit";
            this.StockEdit.UseVisualStyleBackColor = true;
            // 
            // StockBack
            // 
            this.StockBack.Location = new System.Drawing.Point(873, 774);
            this.StockBack.Name = "StockBack";
            this.StockBack.Size = new System.Drawing.Size(75, 23);
            this.StockBack.TabIndex = 37;
            this.StockBack.Text = "Back";
            this.StockBack.UseVisualStyleBackColor = true;
            this.StockBack.Click += new System.EventHandler(this.StockBack_Click);
            // 
            // txtStockMax
            // 
            this.txtStockMax.AutoSize = true;
            this.txtStockMax.Location = new System.Drawing.Point(311, 191);
            this.txtStockMax.Name = "txtStockMax";
            this.txtStockMax.Size = new System.Drawing.Size(155, 13);
            this.txtStockMax.TabIndex = 38;
            this.txtStockMax.Text = "Maximum 50 Characters: (0/50)";
            // 
            // ManageEquipment
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(984, 811);
            this.Controls.Add(this.txtStockMax);
            this.Controls.Add(this.StockBack);
            this.Controls.Add(this.StockEdit);
            this.Controls.Add(this.StockFuelType);
            this.Controls.Add(this.StockPriceMonth);
            this.Controls.Add(this.StockLevel);
            this.Controls.Add(this.StockMake);
            this.Controls.Add(this.StockModel);
            this.Controls.Add(this.StockPriceDay);
            this.Controls.Add(this.StockPriceWeek);
            this.Controls.Add(this.lblStockMake);
            this.Controls.Add(this.lblStockModel);
            this.Controls.Add(this.lblEquipPriceDay);
            this.Controls.Add(this.lblStockPriceWeek);
            this.Controls.Add(this.lblStockPriceMonth);
            this.Controls.Add(this.lblStockLevel);
            this.Controls.Add(this.lblStockPriceFuelType);
            this.Controls.Add(this.btnDelete);
            this.Controls.Add(this.btnSave);
            this.Controls.Add(this.listStock);
            this.Controls.Add(this.lblManageStock);
            this.Controls.Add(this.StockDescr);
            this.Controls.Add(this.StockName);
            this.Controls.Add(this.lblStockDescription);
            this.Controls.Add(this.lblStockName);
            this.Name = "ManageEquipment";
            this.Text = "ManageEquipment";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblStockName;
        private System.Windows.Forms.Label lblStockDescription;
        private System.Windows.Forms.TextBox StockName;
        private System.Windows.Forms.RichTextBox StockDescr;
        private System.Windows.Forms.Label lblManageStock;
        private System.Windows.Forms.ListBox listStock;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.Button btnDelete;
        private System.Windows.Forms.Label lblStockPriceFuelType;
        private System.Windows.Forms.Label lblStockLevel;
        private System.Windows.Forms.Label lblStockPriceMonth;
        private System.Windows.Forms.Label lblStockPriceWeek;
        private System.Windows.Forms.Label lblEquipPriceDay;
        private System.Windows.Forms.Label lblStockModel;
        private System.Windows.Forms.Label lblStockMake;
        private System.Windows.Forms.TextBox StockPriceWeek;
        private System.Windows.Forms.TextBox StockPriceDay;
        private System.Windows.Forms.TextBox StockModel;
        private System.Windows.Forms.TextBox StockMake;
        private System.Windows.Forms.TextBox StockLevel;
        private System.Windows.Forms.TextBox StockPriceMonth;
        private System.Windows.Forms.ComboBox StockFuelType;
        private System.Windows.Forms.Button StockEdit;
        private System.Windows.Forms.Button StockBack;
        private System.Windows.Forms.Label txtStockMax;
    }
}