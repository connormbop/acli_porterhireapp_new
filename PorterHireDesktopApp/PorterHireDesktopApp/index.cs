﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using PorterHireDesktopApp.controller;

namespace PorterHireDesktopApp
{
    public partial class index : Form
    {
        public index()
        {
            InitializeComponent();

            label1.BackColor = System.Drawing.Color.Transparent;
            label2.BackColor = System.Drawing.Color.Transparent;

        }

        private void Loginbtn_Click(object sender, EventArgs e)
        {
            string fName;
            string lName;
            SQL.selectQuery("SELECT A_USERNAME,A_PASSWORD FROM ADMINS");
            if (SQL.read.HasRows)
            {
                while (SQL.read.Read())
                {
                    if (uBox.Text.Equals(SQL.read[0].ToString()) && pBox.Text.Equals(SQL.read[1].ToString()))
                    {
                        //cloggedIn = true;
                        //fName = SQL.read[3].ToString();
                        //lName = SQL.read[2].ToString();
                        new adminMenu().Show();
                        this.Hide();
                    }
                }
            }

            SQL.selectQuery("SELECT U_Username,U_Password FROM USERS");
            if (SQL.read.HasRows)
            {
                while (SQL.read.Read())
                {
                    if (uBox.Text.Equals(SQL.read[0].ToString()) && pBox.Text.Equals(SQL.read[1].ToString()))
                    {
                        //iloggedIn = true;
                        //fName = SQL.read[3].ToString();
                        //lName = SQL.read[2].ToString();
                        new userMenu().Show();
                        this.Hide();
                    }
                }
            }
        }
    }
}
