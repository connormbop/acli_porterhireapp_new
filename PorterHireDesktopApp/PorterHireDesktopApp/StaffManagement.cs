﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PorterHireDesktopApp
{
    public partial class StaffManagement : Form
    {
        public StaffManagement()
        {
            InitializeComponent();

            label1.BackColor = System.Drawing.Color.Transparent;
            label2.BackColor = System.Drawing.Color.Transparent;
            label3.BackColor = System.Drawing.Color.Transparent;
            label4.BackColor = System.Drawing.Color.Transparent;
            label5.BackColor = System.Drawing.Color.Transparent;
            label6.BackColor = System.Drawing.Color.Transparent;
            label7.BackColor = System.Drawing.Color.Transparent;
            label8.BackColor = System.Drawing.Color.Transparent;
            label9.BackColor = System.Drawing.Color.Transparent;
        }

        private void addBtn_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Staff Memeber Added");
            MessageBox.Show("Connection to database failed, Please try again!");
        }

        private void updateBtn_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Staff Memeber updated in database!");
            MessageBox.Show("Connection to database failed, Please try again!");
        }

        private void deleteBtn_Click(object sender, EventArgs e)
        {
            DialogResult dialogResult = MessageBox.Show("Sure", "Warming", MessageBoxButtons.YesNo);
            if (dialogResult == DialogResult.Yes)
            {
                //do something
            }
            else if (dialogResult == DialogResult.No)
            {
                //do something else
            }
        }

        private void button4_Click(object sender, EventArgs e)
        {
            new adminMenu().Show();
            this.Hide();
        }
    }
}
