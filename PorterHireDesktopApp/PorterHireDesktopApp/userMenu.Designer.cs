﻿namespace PorterHireDesktopApp
{
    partial class userMenu
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.cQuoteBtn = new System.Windows.Forms.Button();
            this.vPricingBtn = new System.Windows.Forms.Button();
            this.addClientBtn = new System.Windows.Forms.Button();
            this.logoutBtn = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // cQuoteBtn
            // 
            this.cQuoteBtn.Location = new System.Drawing.Point(398, 178);
            this.cQuoteBtn.Name = "cQuoteBtn";
            this.cQuoteBtn.Size = new System.Drawing.Size(204, 71);
            this.cQuoteBtn.TabIndex = 0;
            this.cQuoteBtn.Text = "Create Quote";
            this.cQuoteBtn.UseVisualStyleBackColor = true;
            this.cQuoteBtn.Click += new System.EventHandler(this.cQuoteBtn_Click);
            // 
            // vPricingBtn
            // 
            this.vPricingBtn.Location = new System.Drawing.Point(398, 312);
            this.vPricingBtn.Name = "vPricingBtn";
            this.vPricingBtn.Size = new System.Drawing.Size(204, 71);
            this.vPricingBtn.TabIndex = 1;
            this.vPricingBtn.Text = "View Pricing";
            this.vPricingBtn.UseVisualStyleBackColor = true;
            this.vPricingBtn.Click += new System.EventHandler(this.vPricingBtn_Click);
            // 
            // addClientBtn
            // 
            this.addClientBtn.Location = new System.Drawing.Point(398, 454);
            this.addClientBtn.Name = "addClientBtn";
            this.addClientBtn.Size = new System.Drawing.Size(204, 71);
            this.addClientBtn.TabIndex = 2;
            this.addClientBtn.Text = "Add Client";
            this.addClientBtn.UseVisualStyleBackColor = true;
            this.addClientBtn.Click += new System.EventHandler(this.addClientBtn_Click);
            // 
            // logoutBtn
            // 
            this.logoutBtn.Location = new System.Drawing.Point(783, 738);
            this.logoutBtn.Name = "logoutBtn";
            this.logoutBtn.Size = new System.Drawing.Size(166, 59);
            this.logoutBtn.TabIndex = 3;
            this.logoutBtn.Text = "Logout";
            this.logoutBtn.UseVisualStyleBackColor = true;
            this.logoutBtn.Click += new System.EventHandler(this.logoutBtn_Click);
            // 
            // userMenu
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(984, 811);
            this.Controls.Add(this.logoutBtn);
            this.Controls.Add(this.addClientBtn);
            this.Controls.Add(this.vPricingBtn);
            this.Controls.Add(this.cQuoteBtn);
            this.Name = "userMenu";
            this.Text = "userMenu";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button cQuoteBtn;
        private System.Windows.Forms.Button vPricingBtn;
        private System.Windows.Forms.Button addClientBtn;
        private System.Windows.Forms.Button logoutBtn;
    }
}